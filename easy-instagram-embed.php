<?php
/*
Plugin Name:    Easy Instagram Embed
Plugin URI:     https://bitbucket.org/kirillmamaev/easy-instagram-embed
Description:    Easy Instagram Embed is a Wordpress plugin that replaces Instagram picture (video) URLs inserted in your Wordpress posts as [embed]https://www.instagram.com/p/8ud2-Xjc1V[/embed] with nice looking Instagram Embeds. <strong><a href="options-general.php?page=easy-instagram-embed-settings">Plugin Settings</a></strong>
Version:        0.9
Author:         Kirill Mamaev
Author URI:     http://kirill.mamaev.net
License:        GPLv2 or later.
License URI:    https://www.gnu.org/licenses/gpl-2.0.html

Easy Instagram Embed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Easy Instagram Embed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with Easy Instagram Embed. If not,
see https://www.gnu.org/licenses/gpl-2.0.html
*/

// Blocks direct access to plugin
defined( 'ABSPATH' ) or die( 'Access denied!' );

// Easy Instagram Embed plugin class
Class Easy_Instagram_Embed {

    function __construct() {
        // Registers plugin settings page and menu item
        add_action( 'admin_init', array( $this, 'init_settings' ) );
        add_action( 'admin_menu', array( $this, 'init_menu' ) );

        // Registers handler to look for Instagram URL embeds
        // regex - '#http(?:s|):\/\/(?:www.|)instagram.com\/p\/(.+)(?:\/|)#i'
        wp_embed_register_handler(
            'easy_instagram_embed',
            '#http(?:s|):\/\/(?:www.|)instagram.com\/p\/(.+)(?:\/|)#i',
            array( $this, 'embed_instagram' )
        );
    }
    
    /**
     * Initialises plugin settings page
     */
    function init_settings() {
        add_settings_section(
            'easy_instagram_embed_settings_section',
            null,
            null,
            'easy-instagram-embed-settings'
        );

        add_settings_field(
            'easy_instagram_embed_size',
            __( 'Instagram Embed width', 'easy_instagram_embed' ),
            array( $this, 'size_field_settings_callback' ),
            'easy-instagram-embed-settings',
            'easy_instagram_embed_settings_section'
        );

        register_setting(
            'easy-instagram-embed-settings',
            'easy_instagram_embed_size'
        );

        add_settings_field(
            'easy_instagram_embed_display',
            __( 'Instagram Embed display options', 'easy_instagram_embed' ),
            array( $this, 'display_field_settings_callback' ),
            'easy-instagram-embed-settings',
            'easy_instagram_embed_settings_section'
        );
        
        register_setting(
            'easy-instagram-embed-settings',
            'easy_instagram_embed_display'
        );
    }

    /**
     * Initialises plugin settings menu item 'Settings > Easy Instagram'
     */
    function init_menu() {
        add_options_page(
            __( 'Easy Instagram Embed Settings', 'easy_instagram_embed' ),
            __( 'Easy Instagram', 'easy_instagram_embed' ),
            'manage_options',
            'easy-instagram-embed-settings',
            array( $this, 'print_settings_page' )
        );
    }

    /**
     * Callback function that prints HTML code for the size settings field
     */
    function size_field_settings_callback() {
        // Get current size or set 300 as default
        $size = get_option( 'easy_instagram_embed_size', 300 );

        echo '<input name="easy_instagram_embed_size" type="number" '
            . 'value="' . $size . '" /> px<br>'
            . '<p class="description">'
            . __( 'There is no height setting because the height will adjust '
                . 'automatically based on the width.',
                'easy_instagram_embed' )
            . '</p>';
    }

    /**
     * Callback function that prints HTML code for the display settings field
     */
    function display_field_settings_callback() {
        // Get current display option or set 2 as default
        $display = get_option( 'easy_instagram_embed_display', 2 );

        echo '<input name="easy_instagram_embed_display" type="radio" '
            . 'value="0" ' . checked( 0, $display, false ) . ' /> With caption'
            . '<br><input name="easy_instagram_embed_display" type="radio" '
            . 'value="1" ' . checked( 1, $display, false ) . ' /> No caption'
            . '<br><input name="easy_instagram_embed_display" type="radio" '
            . 'value="2" ' . checked( 2, $display, false ) . ' /> Image only';
    }

    /**
     * Prints HTML code for the settings page
     */
    function print_settings_page() {
        // Checks for users permissions to manage options
        if ( ! current_user_can( 'edit_theme_options' ) ) {
            wp_die(
                __( 'You do not have sufficient permissions '
                    . 'to access this page.',
                    'easy_instagram_embed' )
            );
        }

        echo '<div class="wrap">'
            . '<h1>'
            . __( 'Easy Instagram Embed Settings', 'easy_instagram_embed' )
            . '</h1>'
            . '<form method="post" action="options.php">';

        settings_fields( 'easy-instagram-embed-settings' );
        do_settings_sections( 'easy-instagram-embed-settings' );
        submit_button();
        
        echo '</form></div>';
    }

    /**
     * Prints Instagram embed HTML code
     */
    function embed_instagram( $matches, $attr, $url, $rawattr ) {
        $image_id = $matches[1];
        $size = get_option( 'easy_instagram_embed_size', 300 );
        $display_option = get_option( 'easy_instagram_embed_display', 2 );
        $embed_html_code = $this->get_HTML_embed_code(
            $image_id,
            $size,
            $display_option
        );
        
        return $embed_html_code;
    }

    /**
     * Gets HTML embed code from Instagram or builds an Image HTML code
     */
    function get_HTML_embed_code(
        $image_id = null,
        $size = 300,
        $display_option = 1
    ) {
        $hide_caption = 1;
        if ( 0 == $display_option ) {
            $hide_caption = 0;
        }
        
        $image_size = $size;
        if ( $size < 320 ) {
            $size = 320;
        }

        /*
        // [DEBUGING ONLY] - Alternative way to request the URL
        $result = json_decode(
            file_get_contents(
                'https://api.instagram.com/oembed?url=http://instagr.am/p/'
                . $image_id . '&maxwidth=' . $size
                . '&hidecaption=' . $hide_caption
            )
        );
        return $result->html;
        */

        $curl = curl_init();
        $curl_options = array(
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_URL => 'https://api.instagram.com/oembed/?url='
                . 'http://instagr.am/p/' . $image_id
                . '&maxwidth=' . $size
                . '&hidecaption=' . $hide_caption,
            CURLOPT_USERAGENT => 'Wordpress'
        );

        curl_setopt_array( $curl, $curl_options );

        $result = json_decode( curl_exec( $curl ) );
        $http_status = curl_getinfo( $curl, CURLINFO_HTTP_CODE );

        curl_close( $curl );

        if ( $http_status === 200 ) {
            if ( $display_option == 2) {
                // Return Image only HTML code
                return '<img class="instagram-media" src="'
                    . $result->thumbnail_url
                    .'">';
            }
            else {
                // Return full HTML
                return str_replace(
                    'margin: 1px;',
                    'margin: 0 10px 10px 0;',
                    $result->html
                );
            }
        } else {
            // Return link only HTML code
            return '<a href="https://www.instagram.com/p/' . $image_id . '">'
                . 'https://www.instagram.com/p/' . $image_id . '</a>';
        }
    }
}

// Initiation
new Easy_Instagram_Embed;