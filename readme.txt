=== Easy Instagram Embed ===
Contributors: kirillmamaev
Tags: instagram embed, instagram, embed, picture, image, video, plugin
Requires at least: 3.5
Tested up to: 4.4
Stable tag: 0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Replaces Instagram picture (video) URLs with nice looking Instagram Embeds.


== Description ==

Easy Instagram Embed is a Wordpress plugin that replaces Instagram picture or 
video URLs inserted in your Wordpress posts as 
[embed]https://www.instagram.com/p/8ud2-Xjc1V[/embed] 
with nice looking Instagram Embeds. 

On the Settings page you can set the width of the embed while the height 
will be automatically calculated to keep the ratio. Also you can set 
the embed display option to show caption, to hide caption or 
to show image only content (default).

In case of any issues please report via the support section.

Plugin code is available in Bitbucket: 
https://bitbucket.org/kirillmamaev/easy-instagram-embed


== Installation ==

1. Upload the plugin files to the '/wp-content/plugins/plugin-name' directory, 
   or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the 'Settings' -> 'Easy Instagram' screen to configure the plugin


== Screenshots ==

1. Easy Instagram Embed settings screen


== Changelog ==

= 0.9 =
* Release candidate 0.9

